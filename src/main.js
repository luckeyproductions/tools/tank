import Vue from 'vue'

import AppLayout from './layout/index.vue'
import router from './router'
import store from './store'

import './mixins'
import './plugins'
import './thirdParty'

import './scss/style.scss'
import { ProjectLoader } from '@/services/project-loader.service'
import { DownloadService } from '@/services/download.service'
import {
  DOWNLOAD_SAMPLE,
  IDE_LOADING,
  CHANGE_PROJECT,
  RESTART_PROJECT,
  RESTART_PLAYER
} from '@/events/events'
import { LocalStorageService } from '@/services/local-storage.service'

Vue.config.productionTip = false

new Vue({
  name: 'Root',
  router,
  store,
  data () {
    return {
      ideLoading: true
    }
  },
  methods: {
    onRemoteEvent (event) {
      if (event.data.name) {
        console.log('event incoming', event.data.name)
        this.$bus.$emit(event.data.name, event.data.payload)
        if (event.data.name === 'engine-message' && event.data.payload.includes('ERROR:') && !event.data.payload.includes('Could not find resource')) {
          this.$store.commit('toast/NEW', {
            message: event.data.payload,
            duration: 2000,
            type: 'error'
          })
        }
      }
    },
    loadFromExternalRepo () {
      return ProjectLoader.LoadProject(this.$route.query, this.$store, this.$bus, () => {
        this.$bus.$emit(IDE_LOADING, false)
      })
    }
  },
  mounted () {
    store.commit('dom/SET_WINDOW_WIDTH', window.innerWidth)
    window.addEventListener('resize', () => store.commit('dom/SET_WINDOW_WIDTH', window.innerWidth))
    if (window.addEventListener) {
      window.addEventListener('message', this.onRemoteEvent, false)
    } else if (window.attachEvent) {
      window.attachEvent('onmessage', this.onRemoteEvent, false)
    }

    this.$bus.$emit(IDE_LOADING, this.loadFromExternalRepo())

    this.$bus.$on(DOWNLOAD_SAMPLE, () => {
      DownloadService.DownloadProject(this.$bus)
    })

    this.$bus.$on(CHANGE_PROJECT, (name) => {
      LocalStorageService.SetProject(name).then(() => {
        this.$bus.$emit(RESTART_PROJECT)
        this.$bus.$emit(RESTART_PLAYER)
      })
    })
  },

  beforeDestroy () {
    window.removeEventListener('resize', () => store.commit('dom/SET_WINDOW_WIDTH', window.innerWidth))
  },
  render: h => h(AppLayout)
}).$mount('#app')
