import { LocalStorageService } from '@/services/local-storage.service'
import { OPEN_FILE } from '../events/events'

let filesToDownload = 0

export class ProjectLoader {
  static BuildUrl (repository, branch, file) {
    return 'https://raw.githubusercontent.com/' + repository + '/' + branch + '/' + file
  }

  static LoadProject ({ repository, provider, branch }, $store, $bus, callback) {
    if (!repository || !provider) {
      return false
    }
    console.log('ProjectLoader::LoadProject::repository', repository)
    console.log('ProjectLoader::LoadProject::branch', branch)

    if (!branch) {
      branch = 'main'
    }
    LocalStorageService.ClearFiles()
    fetch(this.BuildUrl(repository, branch, 'urho_tank.json'))
      .then(response => response.json())
      .then(data => {
        $store.commit('toast/NEW', {
          message: 'Loaded project: ' + data.name,
          duration: 10000,
          type: 'success'
        })
        console.log('ProjectLoader:: Received data', data)
        LocalStorageService.SetProject(data.name)
        LocalStorageService.ClearFiles().then(r => {})
        if (data.files) {
          data.files.forEach(item => {
            this.LoadSingleFile(item, this.BuildUrl(repository, branch, item), $bus, callback)
          })
        }
      }).catch(e => {
        $store.commit('toast/NEW', {
          message: 'Unable to download project!',
          duration: 2000,
          type: 'error'
        })
        console.error(e)
      })
    return true
  }

  static LoadSingleFile (file, url, $bus, callback) {
    // if (file.endsWith('.js')) {
    //   setTimeout(() => {
    //     this.LoadSingleFile(file, url, $bus, callback)
    //   }, 1000)
    // }
    filesToDownload++

    // let type = 'arrayBuffer'
    // const txtFiles = ['.as', '.js', '.xml', '.json', '.glsl']
    // for (let i = 0; i < txtFiles.length; i++) {
    //   if (file.endsWith(txtFiles[i])) {
    //     type = 'text'
    //   }
    // }
    //
    console.log('Downloading single file', url)
    fetch(url)
      .then(response => response.arrayBuffer())
      .then(data => {
        LocalStorageService.UpdateFile(file, data).then(() => {
          setTimeout(() => {
            $bus.$emit(OPEN_FILE, file)
            try {
              console.log('uploading file', file)
              document.getElementById('frame').contentWindow.Module.AddResource(file, data)
            } catch (e) {
              console.error('File upload error', e)
            }
          }, 3000)
          filesToDownload--
          if (filesToDownload === 0) {
            callback()
          }
        }).catch((e) => {
          console.log('Unable to save downloaded file', e)
        })
      }).catch(e => {
        filesToDownload--
        if (filesToDownload === 0) {
          callback()
        }
      })
  }
}
