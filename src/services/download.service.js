import { LocalStorageService } from '@/services/local-storage.service'
import JSZip from 'jszip'
import saveAs from '@/plugins/fileSaver'
import { IDE_LOADING } from '@/events/events'

// eslint-disable-next-line no-unused-vars
let filesLoaded = 0
// eslint-disable-next-line no-unused-vars
let requiredFiles = 0
let $bus = false
// eslint-disable-next-line no-unused-vars
let zip = false

export class DownloadService {
  static onFileLoaded () {
    filesLoaded++
    console.log('Generating zip archive', filesLoaded, requiredFiles)
    if (filesLoaded === requiredFiles && zip) {
      zip.generateAsync({ type: 'blob' })
        .then((content) => {
          // see FileSaver.js
          saveAs(content, 'project.zip')
          console.log('Zip ready ....')
          $bus.$emit(IDE_LOADING, false)
        })
    }
  }

  static fetchAndAddTextFile (filepath, resultName) {
    requiredFiles++
    fetch('/' + filepath).then(response => {
      response.text().then(text => {
        if (resultName) {
          zip.file(resultName, text)
        } else {
          zip.file(filepath, text)
        }
        this.onFileLoaded()
      })
    })
  }

  static fetchAndAddBinaryFile (filepath) {
    requiredFiles++
    fetch('/' + filepath).then(response => {
      response.blob().then(content => {
        console.log('Adding ' + filepath + ' binary file to archive')
        zip.file(filepath, content)
        setTimeout(() => {
          this.onFileLoaded()
        }, 5000)
      })
    })
  }

  static AddGeneratedFiles () {
    const metadata = {
      name: LocalStorageService.GetProject(),
      files: []
    }
    LocalStorageService.GetFiles().then((files) => {
      requiredFiles += files.length
      files.forEach((item) => {
        metadata.files.push(item.filename)
      })

      zip.file('urho_tank.json', JSON.stringify(metadata))
      console.log('Getting file', files)
      files.forEach((file) => {
        LocalStorageService.GetFile(file.filename).then(item => {
          zip.file('data/' + file.filename, item.content)
          this.onFileLoaded()
        })
      })
    })
  }

  static DownloadProject ($busHandler) {
    $bus = $busHandler
    filesLoaded = 0
    requiredFiles = 0
    zip = new JSZip()
    this.AddGeneratedFiles()
    this.fetchAndAddTextFile('player.html', 'index.html')
    this.fetchAndAddBinaryFile('55_DynamicResourceCache.data')
    this.fetchAndAddBinaryFile('55_DynamicResourceCache.wasm')
    // this.fetchAndAddTextFile('55_DynamicResourceCache.wasm.map')
    this.fetchAndAddTextFile('55_DynamicResourceCache.js')
    $bus.$emit(IDE_LOADING, true)
  }
}
