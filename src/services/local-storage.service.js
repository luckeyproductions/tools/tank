import * as Database from '@/plugins/database'

let project = 'first-project'

Database.InitDatabase().then(() => {
  LocalStorageService.SetProject('first-project')
})

export class LocalStorageService {
  static GetProject () {
    return project
  }

  static SetProject (name) {
    project = name
    return Database.SaveProject(name).then(r => {})
  }

  static AddFile (filename, content) {
    return new Promise((resolve) => {
      if (typeof content === 'string') {
        Database.ConvertStringToArrayBuffer(content).then((data) => {
          Database.SaveFile(project, filename, data).then(() => {
            resolve()
          })
        })
      } else {
        Database.SaveFile(project, filename, content).then(() => {
          resolve()
        })
      }
    })
  }

  static GetFile (filename) {
    return Database.GetFile(project, filename)
  }

  static RemoveFile (filename) {
    return Database.DeleteFile(project, filename)
  }

  static ClearFiles () {
    return Database.DeleteAllFiles(project)
  }

  static GetFiles () {
    return Database.GetAll(project)
  }

  static UpdateFile (filename, content) {
    return new Promise((resolve) => {
      if (typeof content === 'string') {
        Database.ConvertStringToArrayBuffer(content).then((data) => {
          Database.UpdateFile(project, filename, data).then(() => {
            resolve()
          })
        })
      } else {
        Database.UpdateFile(project, filename, content).then(() => {
          resolve()
        })
      }
    })
  }
}
