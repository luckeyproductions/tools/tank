/* eslint-disable */
// prefixes of implementation that we want to test
let indexedDB = window.indexedDB || window.mozIndexedDB ||
  window.webkitIndexedDB || window.msIndexedDB

// prefixes of window.IDB objects
let IDBTransaction = window.IDBTransaction ||
  window.webkitIDBTransaction || window.msIDBTransaction
let IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange ||
  window.msIDBKeyRange

if (!window.indexedDB) {
  console.error('Your browser doesn\'t support a stable version of IndexedDB.')
}

Object.defineProperty(window, 'indexedDB', indexedDB);
Object.defineProperty(window, 'IDBTransaction', IDBTransaction);
Object.defineProperty(window, 'IDBKeyRange', IDBKeyRange);

let db

export function ConvertArrayBufferToString(arrayBuffer) {
  return new Promise((resolve) => {
      const blob = new Blob([new Uint8Array(arrayBuffer, arrayBuffer.byteOffset, arrayBuffer.byteLength)]);
      const reader = new FileReader();

      reader.addEventListener('loadend', (e) => {
        resolve(reader.result)
      });

      reader.readAsText(blob);
  });
}

export function ConvertStringToArrayBuffer(str) {
  let blob = new Blob([str], { type: 'octet/stream' })
  return blob.arrayBuffer()
}

export function InitDatabase (name, version) {
  return new Promise((resolve, reject) => {
    console.log('Initializing database')
    if (db) {
      return
    }
    const request = window.indexedDB.open('project_files', 7)

    request.onerror = function (event) {
      console.log('error: ', event)
    }

    request.onsuccess = function (event) {
      db = request.result
      console.log('DB created!', db)
      resolve()
    }

    request.onupgradeneeded = function (event) {
      console.log('Upgrade needed!')
      db = event.target.result
      try {
        db.deleteObjectStore('files')
      } catch (e) {
        //
      }
      db.createObjectStore('files', {keyPath: 'idx'})
      db.createObjectStore('projects', {keyPath: 'name'})
      resolve()
    }

  })
}

export function GetFileIndex(project, filename) {
  return project + ':' + filename
}

export function GetFile(project, filename) {
  return new Promise((resolve, reject) => {
    if (!db) {
      reject()
      return
    }
    var transaction = db.transaction(['files'])
    var objectStore = transaction.objectStore('files')
    var request = objectStore.get(GetFileIndex(project, filename))

    request.onerror = function(event) {
      console.error('Unable to read file from database', filename)
      reject(event)
    }

    request.onsuccess = function(event) {
      if(request.result) {
        resolve(request.result)
      } else {
        reject()
      }
    }
  })
}

export function GetAll(project) {
  return new Promise((resolve, reject) => {
    if (!db) {
      reject()
      return
    }
    var objectStore = db.transaction('files').objectStore('files')

    let files = []
    objectStore.openCursor().onsuccess = function(event) {
      var cursor = event.target.result

      if (cursor) {
        if (project === cursor.value.project) {
          files.push(cursor.value)
        }
        // alert('Name for id ' + cursor.key + ' is ' + cursor.value.name + ',
        //    Age: ' + cursor.value.age + ', Email: ' + cursor.value.email)
        cursor.continue()
      } else {
        resolve(files)
      }
    }
  })
}

export function SaveFile(project, filename, content) {
  return new Promise((resolve, reject) => {
    if (!db) {
      reject()
      return
    }
    const request = db.transaction(['files'], 'readwrite')
      .objectStore('files')
      .add({ idx: GetFileIndex(project, filename), filename: filename, project: project, content: content })

    request.onsuccess = function(event) {
      // alert('Kenny has been added to your database.')
      console.log(filename, 'File added to database')
      resolve(filename)
    }

    request.onerror = function(event) {
      // alert('Unable to add data\r\nKenny is aready exist in your database! ')
      console.error('Unable to add file to the database: ' + filename)
      console.error(event)
      reject()
    }
  })
}

export function DeleteFile(project, filename) {
  return new Promise((resolve, reject) => {
    if (!db) {
      reject()
      return
    }
    const request = db.transaction(['files'], 'readwrite')
      .objectStore('files')
      .delete(GetFileIndex(project, filename))

    request.onsuccess = function (event) {
      console.log('Item', filename, 'has been removed from the database')
      resolve()
    }

    request.onerror = function (e) {
      reject(e)
    }
  })
}

export function DeleteAllFiles(project) {
  return new Promise((resolve, reject) => {
    GetAll(project).then((files) => {
      files.forEach((item) => {
        DeleteFile(project, item.filename)
      })

      resolve()
    })
  })
}

export function UpdateFile(project, filename, content) {
  return new Promise((resolve, reject) => {
    GetFile(project, filename).then((file) => {
      file.content = content
      const request = db.transaction(['files'], 'readwrite')
        .objectStore('files')
        .put(file)

      request.onsuccess = function(e){
        resolve()
      };

      request.onerror = function (e) {
        reject(e)
      }
    }).catch(e => {
      SaveFile(project, filename, content).then(() => {
        resolve()
      })
    })
  })
}

function AddProject(project, resolve, reject) {
  const request = db.transaction(['projects'], 'readwrite')
    .objectStore('projects')
    .add({ name: project })

  request.onsuccess = function(event) {
    // alert('Kenny has been added to your database.')
    console.log('Project added to database: ', project)
    resolve()
  }

  request.onerror = function(event) {
    // alert('Unable to add data\r\nKenny is aready exist in your database! ')
    console.error('Unable to add project: ' + project)
    console.error(event)
    reject()
  }
}

export function SaveProject(project) {
  return new Promise((resolve, reject) => {
    if (!db) {
      reject()
      return
    }

    var transaction = db.transaction(['projects'])
    var objectStore = transaction.objectStore('projects')
    var request = objectStore.get(project)

    request.onerror = function(event) {
      AddProject(project, resolve, reject)
    }

    request.onsuccess = function(event) {
      if(request.result) {
        console.log('project already added')
        resolve()
      } else {
        AddProject(project, resolve, reject)
      }
    }
  })
}
