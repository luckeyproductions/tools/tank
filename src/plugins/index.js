/**
 * import and init global plugins
 */

import Vue from 'vue'

import globalEventBus from '../plugins/globalEventBus'
import shortkey from 'vue-shortkey'

Vue.use(globalEventBus)
Vue.use(shortkey)
